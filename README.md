django-cms-plugins
====================

Plugins for Django CMS

Authors
-------
*  Ondrej Sika, <http://ondrejsika.com>, dev@ondrejsika.com

Source
------
* Documentation: <http://ondrejsika.com/docs/django-cms-plugins>
* Python Package Index: <http://pypi.python.org/pypi/django-cms-plugins>
* GitHub: <https://github.com/sikaondrej/django-cms-plugins>
