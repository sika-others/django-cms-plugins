import random

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _

from models import GalleryPlugin


class GalleryPluginX(CMSPluginBase):
    model = GalleryPlugin
    name = _("Gallery X")
    render_template = "cms_plugins/gallery.html"

    def render(self, context, instance, placeholder):
        context.update({
            'object':instance.gallery,
            'instance':instance,
            'placeholder':placeholder,
            "random": random.randint(0, 999999),
        })  
        return context


plugin_pool.register_plugin(GalleryPluginX)
