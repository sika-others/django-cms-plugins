from django.db import models
from cms.models import CMSPlugin, Page


GALLERY_TYPES = (("gallery", "Gallery"), ("slider", "Slider"), )


class Gallery(models.Model):
    gallery_type = models.CharField(max_length=64, choices=GALLERY_TYPES)
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.gallery_type)

class Image(models.Model):
    gallery = models.ForeignKey(Gallery)
    img = models.ImageField(upload_to="cmsweb/image/img")

class GalleryPlugin(CMSPlugin):
    gallery = models.ForeignKey(Gallery)

    def __unicode__(self):
        return u"%s ~%i" % (self.gallery.name, self.gallery.pk)
