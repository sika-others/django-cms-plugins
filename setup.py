#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "django-cms-plugins",
    version = "1.0.1",
    url = 'http://ondrejsika.com/docs/django-cms-plugins/',
    download_url = 'https://github.com/sikaondrej/django-cms-plugins/',
    license = 'GNU LGPL v.3',
    description = "",
    author = 'Ondrej Sika',
    author_email = 'dev@ondrejsika.com',
    packages = find_packages(),
    install_requires = ["django-cms", "django-sekizai"],
    include_package_data = True,
    zip_safe = False,
)
